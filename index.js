let http = require("http");

// Create server
http.createServer(function (request, response) {
	response.writeHead(200, {'Content-Type': 'text/plain'});

	response.end("Hello world!");
}).listen(4000);

console.log('Server running at localhost:4000');