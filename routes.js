const http = require('http');

// Creates a variable "port" to store the port number
const port = 4000;

// Creates a variable "server" that stores the output of the "createServer" method
const server = http.createServer((request, response) => {
	// localhost:4000/greeting
	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Hello Again!');
	}
	// localhost:4000/homepage
	else if(request.url == '/homepage'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('This is homepage!');
	}
	else{
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page is not available');
	}
});
	
server.listen(port);

console.log(`Server is now accessible at localhost:${port}.`);
